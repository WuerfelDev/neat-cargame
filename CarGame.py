import pygame
import os
import math
import sys
import random
import neat

# Resolution of map image
screen_width = 1500
screen_height = 800

generation = -1 #to match the console output
toggles = {'show_radar': True,'first_car': False,'distance': True}


class Car:
    def __init__(self):
        self.surface = pygame.image.load("img/car.png")
        self.surface = pygame.transform.scale(self.surface, (100, 100))
        self.regularcar = self.surface
        self.firstcar = pygame.image.load("img/car_first.png")
        self.firstcar = pygame.transform.scale(self.firstcar, (100, 100))
        self.rotate_surface = self.surface
        self.pos = [700, 650]
        self.angle = 0
        self.speed = 0
        self.center = [self.pos[0] + 50, self.pos[1] + 50]
        self.radars = []
        self.radars_for_draw = []
        self.is_alive = True
        self.goal = False
        self.distance = 0
        self.time_spent = 0

    def draw(self, screen):
        screen.blit(self.rotate_surface, self.pos)
        global toggles
        if toggles['show_radar']:
            self.draw_radar(screen)

        # Write distance on car
        if toggles['distance']:
            font = pygame.font.SysFont("Arial", 20)
            text = font.render(str(int(self.distance)), True, (0,0,0))
            text_rect = text.get_rect(center=self.center)
            pygame.draw.rect(screen, (201,0,0),text_rect)
            screen.blit(text, text_rect)

    def draw_radar(self, screen):
        for r in self.radars:
            pos, dist = r
            pygame.draw.line(screen, (0, 255, 0), self.center, pos, 1)
            pygame.draw.circle(screen, (0, 255, 0), pos, 4)

    def check_collision(self, map):
        self.is_alive = True
        for p in self.four_points:
            if map.get_at((int(p[0]), int(p[1]))) == (255, 255, 255, 255):
                self.is_alive = False
                break

    def check_radar(self, degree, map):
        len = 0
        x = int(self.center[0] + math.cos(math.radians(360 - (self.angle + degree))) * len)
        y = int(self.center[1] + math.sin(math.radians(360 - (self.angle + degree))) * len)

        while not map.get_at((x, y)) == (255, 255, 255, 255) and len < 300:
            len = len + 1
            x = int(self.center[0] + math.cos(math.radians(360 - (self.angle + degree))) * len)
            y = int(self.center[1] + math.sin(math.radians(360 - (self.angle + degree))) * len)

        dist = int(math.sqrt(math.pow(x - self.center[0], 2) + math.pow(y - self.center[1], 2)))
        self.radars.append([(x, y), dist])

    def update(self, map, first):
        
        if first:
            self.surface = self.firstcar
        else:
            self.surface = self.regularcar
        
        #check position
        self.rotate_surface = self.rot_center(self.surface, self.angle)
        self.pos[0] += math.cos(math.radians(360 - self.angle)) * self.speed
        if self.pos[0] < 20:
            self.pos[0] = 20
        elif self.pos[0] > screen_width - 120:
            self.pos[0] = screen_width - 120

        self.distance += self.speed
        self.time_spent += 1
        self.pos[1] += math.sin(math.radians(360 - self.angle)) * self.speed
        if self.pos[1] < 20:
            self.pos[1] = 20
        elif self.pos[1] > screen_height - 120:
            self.pos[1] = screen_height - 120

        # caculate 4 collision points
        self.center = [int(self.pos[0]) + 50, int(self.pos[1]) + 50]
        len = 40
        left_top = [self.center[0] + math.cos(math.radians(360 - (self.angle + 30))) * len, self.center[1] + math.sin(math.radians(360 - (self.angle + 30))) * len]
        right_top = [self.center[0] + math.cos(math.radians(360 - (self.angle + 150))) * len, self.center[1] + math.sin(math.radians(360 - (self.angle + 150))) * len]
        left_bottom = [self.center[0] + math.cos(math.radians(360 - (self.angle + 210))) * len, self.center[1] + math.sin(math.radians(360 - (self.angle + 210))) * len]
        right_bottom = [self.center[0] + math.cos(math.radians(360 - (self.angle + 330))) * len, self.center[1] + math.sin(math.radians(360 - (self.angle + 330))) * len]
        self.four_points = [left_top, right_top, left_bottom, right_bottom]

        self.check_collision(map)
        self.radars.clear()
        for d in range(-90, 120, 45):
            self.check_radar(d, map)

    def get_data(self):
        radars = self.radars
        ret = [0, 0, 0, 0, 0, self.speed]
        for i, r in enumerate(radars):
            ret[i] = int(r[1] / 30)

        return ret

    def get_alive(self):
        if self.time_spent >= 800:
            return False
        return self.is_alive

    def get_reward(self):
            return (self.distance / self.time_spent)* self.distance / 3000 

    def rot_center(self, image, angle):
        orig_rect = image.get_rect()
        rot_image = pygame.transform.rotate(image, angle)
        rot_rect = orig_rect.copy()
        rot_rect.center = rot_image.get_rect().center
        rot_image = rot_image.subsurface(rot_rect).copy()
        return rot_image


def run_car(genomes, config):

    # Init NEAT
    nets = []
    cars = []

    for id, g in genomes:
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nets.append(net)
        g.fitness = 0

        # Init my cars
        cars.append(Car())

    # Init my game
    pygame.init()
    screen = pygame.display.set_mode((screen_width, screen_height))
    pygame.display.set_caption('neat-cargame')
    clock = pygame.time.Clock()
    generation_font = pygame.font.SysFont("Arial", 70)
    font = pygame.font.SysFont("Arial", 30)
    map = pygame.image.load('img/map.png')


    # Main loop
    global generation
    generation += 1

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)

        # Input my data and get result from network
        for index, car in enumerate(cars):
            output = nets[index].activate(car.get_data())
            o = max(output[0:1])
           
            car.angle += output[0]*10
            car.speed += output[1]*5
            if car.speed < 5:
                car.speed = 5


        # Sort cars by distance (if alive)
        indices = sorted(range(len(cars)),key=lambda x: cars[x].distance if cars[x].get_alive() else False)

        # Update car and fitness
        remain_cars = 0
        for i in indices:
            if cars[i].get_alive():
                remain_cars += 1
                cars[i].update(map,i==indices[-1]) #longest distance
                genomes[i][1].fitness += cars[i].get_reward()

        # check
        if remain_cars == 0:
            break

        # Drawing, draw highest distance last/on top
        global toggles
        screen.blit(map, (0, 0))
        for i in indices:
            if cars[i].get_alive():
                if i==indices[-1]  or not toggles['first_car']:
                    cars[i].draw(screen)

        text = generation_font.render("Generation: " + str(generation), True, (255, 255, 0))
        text_rect = text.get_rect()
        text_rect.center = (screen_width/2, 100)
        screen.blit(text, text_rect)

        text = font.render("remaining cars: " + str(remain_cars), True, (0, 0, 0))
        text_rect = text.get_rect()
        text_rect.center = (screen_width/2, 200)
        screen.blit(text, text_rect)

        y = 25
        y += button(screen,"Radar", y,"show_radar")
        y += button(screen,"First Car", y,"first_car")
        y += button(screen,"Distance", y,"distance")

        pygame.display.flip()
        clock.tick(0)


# Left buttons
def button(screen,text,outer_y,toggle_val):
    margin = 7
    padding = 5
    border = 4

    outer_y += margin
    outer_x = margin
    
    font = pygame.font.SysFont("Arial", 20)
    text = font.render(text, True, (0, 0, 0))
    text_rect = text.get_rect(y=outer_y+padding+border,x=outer_x+padding+border)
    x,y,w,h = text_rect
    
    # Border
    border_rect = pygame.Rect(x-padding-border,y-padding-border,w+2*padding+2*border,h+2*padding+2*border)
    pygame.draw.rect(screen, (30,70,200), border_rect)
    
    # Background
    bg_rect = (x-padding,y-padding,w+2*padding,h+2*padding)


    mouse = pygame.mouse.get_pos()
    if border_rect.x+border_rect.w > mouse[0] > border_rect.x and border_rect.y+border_rect.h > mouse[1] > border_rect.y:
        clicked = False
        while pygame.mouse.get_pressed()[0] == 1:
            pygame.event.get()
            clicked = True
        if clicked:
            global toggles
            toggles[toggle_val] = not toggles[toggle_val]
    else:
        pygame.draw.rect(screen, (70,150,240), bg_rect)

    screen.blit(text, text_rect)
    return border_rect.h + 2*margin




if __name__ == "__main__":
    # Set configuration file
    config_path = "./config-feedforward.txt"
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction, neat.DefaultSpeciesSet, neat.DefaultStagnation, config_path)

    # Create core evolution algorithm class
    p = neat.Population(config)

    # Add reporter for fancy statistical result
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    # Run NEAT
    winner = p.run(run_car, 1000)
    print('\nBest genome:\n{!s}'.format(winner))
