# neat-cargame

> Based on https://github.com/monokim/framework_tutorial/tree/master/neat


### What this is

A race track. For neural networks.
![screenshot.png](screenshot.png)

[NEAT](https://neat-python.readthedocs.io/en/latest/neat_overview.html) is a evolutionary algorithm that generates different neural networks (nn) to control a car on the track. Through a reward function (feedback on how good a nn performed) the algorithm evolves newer generations/ new nns to maximize the reward.

A car has 5 radar sensors to know the distance to the edge of the track. Those values as well as its current speed are input values to the neural network. The car then receives a steering value (right/left, up to 10 degrees) and a acceleration/deceleration value (±5px per time frame)


### Modifications

- Dynamic steering (up to 10 degrees)
- Dynamic acceleration and deceleration
- Reward function based on distance and speed
- Show first car in orange
- Display distance of each car
- Toggle options
- Fixes

